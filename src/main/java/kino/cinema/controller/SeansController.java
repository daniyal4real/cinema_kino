package kino.cinema.controller;


import kino.cinema.entity.Seans;
import kino.cinema.service.SeansService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping("/api/seans")
@Slf4j
public class SeansController {
    private final SeansService seansService;


    @RequestMapping
    public List<Seans> getAllSeans() {
        return seansService.getAllSeans();
    }

    @RequestMapping("/{id}")
    public ResponseEntity<?> getSeansById(@PathVariable Integer id) {
        return ResponseEntity.ok(seansService.getSeansById(id));
    }
}
