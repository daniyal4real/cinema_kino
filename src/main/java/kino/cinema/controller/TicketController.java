package kino.cinema.controller;


import kino.cinema.dto.TicketRequest;
import kino.cinema.entity.Ticket;
import kino.cinema.service.TicketService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/ticket")
public class TicketController {


    private final TicketService ticketService;

    @RequestMapping("/{id}")
    public ResponseEntity<?> getTickerById(@PathVariable Integer id) {
        return ResponseEntity.ok(ticketService.getTicketById(id));
    }

    @PostMapping
    public ResponseEntity<?> createTicket(@RequestBody TicketRequest ticketRequest) {
        return ResponseEntity.ok(ticketService.createTicket(ticketRequest));
    }
}
