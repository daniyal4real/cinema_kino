package kino.cinema.repository;

import kino.cinema.entity.Seans;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SeansRepository extends JpaRepository<Seans, Integer> {



}
