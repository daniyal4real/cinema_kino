package kino.cinema.service;


import kino.cinema.entity.Seans;
import kino.cinema.repository.SeansRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class SeansService {

    private final SeansRepository seansRepository;

    public List<Seans> getAllSeans() {
        return seansRepository.findAll();
    }

    public Object getSeansById(Integer id) {
        return seansRepository.findById(id);
    }
}
