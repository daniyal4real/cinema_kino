package kino.cinema.service;


import kino.cinema.dto.TicketRequest;
import kino.cinema.entity.Movie;
import kino.cinema.entity.Seans;
import kino.cinema.entity.Ticket;
import kino.cinema.entity.User;
import kino.cinema.repository.TicketRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class TicketService {

    private final TicketRepository ticketRepository;
    private final MovieService movieService;
    private final UserService userService;
    private final SeansService seansService;

    public ResponseEntity<?> getTicketById(Integer id) {
        return ResponseEntity.ok(ticketRepository.findById(id));
    }

    public Ticket createTicket(TicketRequest ticketRequest) {
        Ticket ticket = new Ticket();
        ticket.setId(ticketRequest.getId());
        ticket.setMovie((Movie) movieService.getMovieById(ticketRequest.getMovieId()));
        ticket.setUser((User) userService.getUserById(ticketRequest.getUserId()));
        ticket.setSeans((Seans) seansService.getSeansById(ticketRequest.getSeansId()));
        return ticketRepository.save(ticket);
    }
}
