package kino.cinema.service;



import kino.cinema.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;


@Service
@RequiredArgsConstructor
public class UserService {

    private final UserRepository userRepository;

    public Object getUserById(Integer id) {
        return userRepository.findById(id);
    }
}
