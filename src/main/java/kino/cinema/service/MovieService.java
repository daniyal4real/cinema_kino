package kino.cinema.service;


import kino.cinema.dto.MovieRequest;
import kino.cinema.entity.Movie;
import kino.cinema.repository.MovieRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class MovieService {

    private final MovieRepository movieRepository;

    public List<Movie> getAllMovies() {
        return movieRepository.findAll();
    }

    public Movie createMovie(MovieRequest movieRequest) {
        Movie movie = Movie.builder()
                .title(movieRequest.getTitle())
                .description(movieRequest.getDescription())
                .rating(movieRequest.getRating())
                .image(movieRequest.getImage())
                .published(movieRequest.getPublished())
                .producer(movieRequest.getProducer())
                .build();
        return movieRepository.save(movie);
    }

    public Object getMovieById(Integer id) {
        return movieRepository.findById(id);
    }

    public Movie updateMovie(Movie movie, Integer id) {
        return movieRepository.save(movie);
    }
}
