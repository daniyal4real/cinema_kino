package kino.cinema.entity;


import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "kinopark_order")
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private Long totalPrice;
    private Time time;
    @OneToMany(targetEntity = Ticket.class)
    List<Ticket> ticketList;

    @OneToOne(targetEntity = User.class)
    User user;
}
