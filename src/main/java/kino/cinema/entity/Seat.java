package kino.cinema.entity;


import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "kinopark_seat")
public class Seat{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private Integer seatNumber;
    private Boolean available;

    @OneToOne(targetEntity = Seans.class)
    private Seans seans;
}
