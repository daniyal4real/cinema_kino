package kino.cinema.entity;


import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "kinopark_ticket")
public class Ticket {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @OneToOne(targetEntity = Movie.class, cascade = CascadeType.ALL)
    private Movie movie;

    @OneToOne(targetEntity = Seans.class, cascade = CascadeType.ALL)
    private Seans seans;

    @OneToOne(targetEntity = User.class, cascade = CascadeType.ALL)
    private User user;
}
