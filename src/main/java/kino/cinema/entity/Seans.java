package kino.cinema.entity;


import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Time;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "kinopark_seans")
public class Seans {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private Integer price;
    private Time time;
    private String language;
    @OneToOne(targetEntity = Kinozal.class)
    private Kinozal kinozal;
    @OneToOne(targetEntity = Movie.class)
    private Movie movie;
}
