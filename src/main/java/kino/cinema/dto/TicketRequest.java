package kino.cinema.dto;


import lombok.Data;

@Data
public class TicketRequest{
    private Integer id;
    private Integer movieId;
    private Integer seansId;
    private Integer userId;
}
